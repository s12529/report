
public class ReportMaker implements Visitor{
	
	private int numberOfClients;
	private int numberOfOrders;
	private int numberOfProducts;
	private String report;
	
	
	public int getNumberOfClients() {
		return numberOfClients;
	}
	
	public int getNumberOfOrders() {
		return numberOfOrders;
	}
	
	public int getNumberOfProducts() {
		return numberOfProducts;
	}
	
	public String showReport() {
		return report;
	}
	

	@Override
	public void visit(Visitable v) {
		StringBuilder builder = new StringBuilder();
		builder.append("\n" + v.giveReport());
		builder.append("\n");
		this.report = builder.toString();
		
		if(v instanceof Product) {}
		
		if(v instanceof Order) {		
			
				numberOfProducts += ((Order) v).getProducts().size();
			
		}
		
		if(v instanceof Client) {
			for(Order orders: ((Client) v).getOrders()) {
				numberOfOrders++;
				numberOfProducts += orders.getProducts().size();
					
			}
			
		}
		
		
		if(v instanceof ClientGroup) {
			for(Client clients: ((ClientGroup) v).getClients()) {		
				numberOfClients++;
				for(Order orders: clients.getOrders()) {
					numberOfOrders++;
					numberOfProducts += orders.getProducts().size();
				}
			
				
			}
		}
		
	}
	
}
