import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Order implements Visitable {

	private String number;
	private double orderTotalPrice;
	private Date orderDate = new Date();
	private List<Product> products = new ArrayList<Product>();
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public double getOrderTotalPrice() {
		return orderTotalPrice;
	}
	
	public Date getOrderDate() {
		return orderDate;
	}

	void addProduct(Product product) {
		products.add(product);
	}
	
	public List<Product> getProducts() {
		return this.products;
	}
		
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
		giveReport();
	}
	
	
	@Override
	public String giveReport() {
		StringBuilder builder = new StringBuilder();
		builder.append("nr. " + getNumber() + ", cena ��czna: " + getOrderTotalPrice() + " zl, data: " + getOrderDate());
		for(Product tmp: products) {
			builder.append("\n" + tmp.giveReport());
		}
		return builder.toString();
	}

	public void totalPrice() {
		for(Product tmp: products) {
			this.orderTotalPrice += tmp.getPrice();
		}
	}

	
	
	
}
