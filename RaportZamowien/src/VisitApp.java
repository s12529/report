public class VisitApp {

	public static void main(String[] args) {
		
		Product p1 = new Product("Ryz", 3);
		Product p2 = new Product("Chleb", 2);
		Product p3 = new Product("Marchewki", 1);
		Product p4 = new Product("Tymbark", 2);
		Product p5 = new Product("Cola", 2.5);
		
		ReportMaker rP = new ReportMaker();
		
		rP.visit(p1);
		
		System.out.println("---------------------------------------------------------Produkt ");
		
		System.out.println(rP.showReport());

		System.out.println("---------------------------------------------------------Zamowienie ");	
		
		Order o1 = new Order();
		o1.setNumber("Zamowienie nr 1");
		o1.addProduct(p5);
		o1.addProduct(p4);
		o1.addProduct(p2);
		o1.totalPrice();
		
		
		ReportMaker rO = new ReportMaker();
		rO.visit(o1);
		System.out.println(rO.showReport());
		System.out.println("Liczba produktow: " + rO.getNumberOfProducts());
		
		System.out.println("---------------------------------------------------------Klient ");
		Client c1 = new Client();
		c1.addOrder(o1);
		Order o2 = new Order();
		o2.setNumber("zamowienie nr 2");
		o2.addProduct(p1);
		o2.addProduct(p3);
		o2.addProduct(p5);
		o2.totalPrice();
		c1.addOrder(o2); 
		c1.setNumber("Klient nr 1");
		
		ReportMaker rC = new ReportMaker();
		
		rC.visit(c1);
		System.out.println(rC.showReport());
		System.out.println("Liczba zamowien: " + rC.getNumberOfOrders());
		System.out.println("Liczba produktow: " + rC.getNumberOfProducts());
		
		System.out.println("---------------------------------------------------------Klient Grupa ");
		
		ReportMaker rCG = new ReportMaker();
		ClientGroup cG1 = new ClientGroup();
		
		cG1.setMemberName("VIP");
		cG1.addClient(c1);
		
		rCG.visit(cG1);
		System.out.println(rCG.showReport());
		System.out.println("Liczba zamowien: " + rCG.getNumberOfOrders());
		System.out.println("Liczba produktow: " + rCG.getNumberOfProducts());
		System.out.println("Liczba klientow: " + rCG.getNumberOfClients());
	
	}
}
