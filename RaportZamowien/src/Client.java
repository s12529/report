import java.util.ArrayList;
import java.util.List;


public class Client implements Visitable{

	private String number;	
	private List<Order> orders = new ArrayList<Order>();
	
	public List<Order> getOrders() {
		return this.orders;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	public void addOrder(Order order) {
		orders.add(order);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);		
	}

	@Override
	public String giveReport() {
		StringBuilder builder = new StringBuilder();	
		builder.append(getNumber());
		for(Order tmp: orders) {
			builder.append("\n" + tmp.giveReport());
		}
		return builder.toString();
	}	
}
