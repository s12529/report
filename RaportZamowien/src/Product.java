                           
public class Product implements Visitable{

	private String name;
	private double price;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);	
	}
	
	@Override
	public String giveReport() {
		StringBuilder builder = new StringBuilder();
		builder.append("Produkt: " + getName() + ", cena: " + getPrice() +" zl.");
		return builder.toString();
	}
	
	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}
	
	
	
}
