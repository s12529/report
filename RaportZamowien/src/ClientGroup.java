import java.util.ArrayList;
import java.util.List;


public class ClientGroup implements Visitable{

	private String memberName;
	private List<Client> clients = new ArrayList<Client>();
	
	void addClient(Client client) {	
		clients.add(client);
	}
	
	
	public List<Client> getClients() {
		return this.clients;
	}
	
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String giveReport() {
		StringBuilder builder = new StringBuilder();
		builder.append("Użytkownik: " + this.memberName);
		for(Client tmp: clients) {
			builder.append("\n" + tmp.giveReport());
		}
		return builder.toString();
	}
}
